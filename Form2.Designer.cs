﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {

        //Get parent so we can easily call the update function
        private Form2 parent;

        public string passHash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public bool checkUsernameAndPassword()
        {
            if (!File.Exists("C:\temp\tunnukset.txt"))
            {
                MessageBox.Show("Username and password file \"username_and_password.txt\" doesnt exist in programs folder!", "Username and password file error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                string[] lines = File.ReadAllLines("C:\temp\tunnukset.txt", Encoding.UTF8);
                if (lines[0] == userName.Text && lines[1] == this.passHash(passWord.Text))
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Wrong username or password!", "Login error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
        }
        private void configCloseBtn_Click(object sender, EventArgs e)
        {
            this.parent.Close();
            this.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.userName = new System.Windows.Forms.TextBox();
            this.passWord = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(129, 63);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(114, 20);
            this.userName.TabIndex = 0;
            // 
            // passWord
            // 
            this.passWord.Location = new System.Drawing.Point(133, 114);
            this.passWord.Name = "passWord";
            this.passWord.Size = new System.Drawing.Size(110, 20);
            this.passWord.TabIndex = 1;
            // 
            // Form2
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.passWord);
            this.Controls.Add(this.userName);
            this.Name = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private TextBox userName;
        private TextBox passWord;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        string diesel, bensa95, bensa98;
        int dieselLitra, bensa95Litra, bensa98Litra;
        string hinnat = "c:/bensa.txt";
        public Form1()
        {
            InitializeComponent();
            luehinnat(hinnat);
        }
        private void callonload()
        {
        }
        private void luehinnat(string lueHinnat)
        {
            string[] lines = File.ReadAllLines("C:/temp/bensa.txt");
            diesel = lines[0];
            bensa95 = lines[1];
            bensa98 = lines[2];
            dieselLitra = Convert.ToInt32(diesel);
            bensa95Litra = Convert.ToInt32(bensa95);
            bensa98Litra = Convert.ToInt32(bensa98);


        }
        private void tilaaLisaaButton_Click(object sender, EventArgs e)
        {
            tilaaLisaaForm tilaaLisaaForm = new tilaaLisaaForm();
            tilaaLisaaForm.Show();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            callonload();
            bensa95Label.Text = bensa95 + " L";
            bensa98Label.Text = bensa98 + " L";
            dieselLabel.Text = diesel + " L";

            if (dieselLitra <= 100)
            {
                dieselVahissaLabel.ForeColor = Color.Red;
            }

            if (bensa98Litra <= 100)
            {
                bensa98VahissaLabel.ForeColor = Color.Red;
            }

            if (bensa95Litra <= 100)
            {
                bensa95VahissaLabel.ForeColor = Color.Red;
            }
        }
    }
}

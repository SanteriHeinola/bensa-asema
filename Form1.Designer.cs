﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tilaaLisaaButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.bensa98Box = new System.Windows.Forms.TextBox();
            this.bensa98VahissaBox = new System.Windows.Forms.Label();
            this.bensa95VahissaBox = new System.Windows.Forms.Label();
            this.bensa95Box = new System.Windows.Forms.TextBox();
            this.dieselBox = new System.Windows.Forms.TextBox();
            this.dieselVahissaBox = new System.Windows.Forms.Label();
            this.bensa95VahissaLabel = new System.Windows.Forms.Label();
            this.bensa98VahissaLabel = new System.Windows.Forms.Label();
            this.dieselVahissaLabel = new System.Windows.Forms.Label();
            this.bensa95Label = new System.Windows.Forms.Label();
            this.bensa98Label = new System.Windows.Forms.Label();
            this.dieselLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Dubai Medium", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 49);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bensa 95";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Dubai Medium", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(341, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 49);
            this.label3.TabIndex = 7;
            this.label3.Text = "Diesel";
            // 
            // tilaaLisaaButton
            // 
            this.tilaaLisaaButton.Font = new System.Drawing.Font("Dubai", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tilaaLisaaButton.Location = new System.Drawing.Point(156, 172);
            this.tilaaLisaaButton.Name = "tilaaLisaaButton";
            this.tilaaLisaaButton.Size = new System.Drawing.Size(189, 61);
            this.tilaaLisaaButton.TabIndex = 11;
            this.tilaaLisaaButton.Text = "Tilaa lisaa";
            this.tilaaLisaaButton.UseVisualStyleBackColor = true;
            this.tilaaLisaaButton.Click += new System.EventHandler(this.tilaaLisaaButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Dubai Medium", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(175, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 49);
            this.label2.TabIndex = 12;
            this.label2.Text = "Bensa 98";
            // 
            // bensa98Box
            // 
            this.bensa98Box.Location = new System.Drawing.Point(0, 0);
            this.bensa98Box.Name = "bensa98Box";
            this.bensa98Box.Size = new System.Drawing.Size(100, 20);
            this.bensa98Box.TabIndex = 17;
            // 
            // bensa98VahissaBox
            // 
            this.bensa98VahissaBox.Location = new System.Drawing.Point(0, 0);
            this.bensa98VahissaBox.Name = "bensa98VahissaBox";
            this.bensa98VahissaBox.Size = new System.Drawing.Size(100, 23);
            this.bensa98VahissaBox.TabIndex = 14;
            // 
            // bensa95VahissaBox
            // 
            this.bensa95VahissaBox.Location = new System.Drawing.Point(0, 0);
            this.bensa95VahissaBox.Name = "bensa95VahissaBox";
            this.bensa95VahissaBox.Size = new System.Drawing.Size(100, 23);
            this.bensa95VahissaBox.TabIndex = 15;
            // 
            // bensa95Box
            // 
            this.bensa95Box.Location = new System.Drawing.Point(0, 0);
            this.bensa95Box.Name = "bensa95Box";
            this.bensa95Box.Size = new System.Drawing.Size(100, 20);
            this.bensa95Box.TabIndex = 18;
            // 
            // dieselBox
            // 
            this.dieselBox.Location = new System.Drawing.Point(0, 0);
            this.dieselBox.Name = "dieselBox";
            this.dieselBox.Size = new System.Drawing.Size(100, 20);
            this.dieselBox.TabIndex = 16;
            // 
            // dieselVahissaBox
            // 
            this.dieselVahissaBox.Location = new System.Drawing.Point(0, 0);
            this.dieselVahissaBox.Name = "dieselVahissaBox";
            this.dieselVahissaBox.Size = new System.Drawing.Size(100, 23);
            this.dieselVahissaBox.TabIndex = 13;
            // 
            // bensa95VahissaLabel
            // 
            this.bensa95VahissaLabel.AutoSize = true;
            this.bensa95VahissaLabel.Font = new System.Drawing.Font("Dubai Medium", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa95VahissaLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.bensa95VahissaLabel.Location = new System.Drawing.Point(15, 23);
            this.bensa95VahissaLabel.Name = "bensa95VahissaLabel";
            this.bensa95VahissaLabel.Size = new System.Drawing.Size(151, 32);
            this.bensa95VahissaLabel.TabIndex = 19;
            this.bensa95VahissaLabel.Text = "Alle 100 Litraa!";
            // 
            // bensa98VahissaLabel
            // 
            this.bensa98VahissaLabel.AutoSize = true;
            this.bensa98VahissaLabel.Font = new System.Drawing.Font("Dubai Medium", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa98VahissaLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.bensa98VahissaLabel.Location = new System.Drawing.Point(178, 24);
            this.bensa98VahissaLabel.Name = "bensa98VahissaLabel";
            this.bensa98VahissaLabel.Size = new System.Drawing.Size(151, 32);
            this.bensa98VahissaLabel.TabIndex = 20;
            this.bensa98VahissaLabel.Text = "Alle 100 Litraa!";
            // 
            // dieselVahissaLabel
            // 
            this.dieselVahissaLabel.AutoSize = true;
            this.dieselVahissaLabel.Font = new System.Drawing.Font("Dubai Medium", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieselVahissaLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.dieselVahissaLabel.Location = new System.Drawing.Point(333, 24);
            this.dieselVahissaLabel.Name = "dieselVahissaLabel";
            this.dieselVahissaLabel.Size = new System.Drawing.Size(151, 32);
            this.dieselVahissaLabel.TabIndex = 21;
            this.dieselVahissaLabel.Text = "Alle 100 Litraa!";
            // 
            // bensa95Label
            // 
            this.bensa95Label.AutoSize = true;
            this.bensa95Label.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bensa95Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bensa95Label.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa95Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bensa95Label.Location = new System.Drawing.Point(21, 93);
            this.bensa95Label.Name = "bensa95Label";
            this.bensa95Label.Size = new System.Drawing.Size(134, 27);
            this.bensa95Label.TabIndex = 25;
            this.bensa95Label.Text = "Bensa 95";
            // 
            // bensa98Label
            // 
            this.bensa98Label.AutoSize = true;
            this.bensa98Label.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bensa98Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bensa98Label.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa98Label.Location = new System.Drawing.Point(184, 93);
            this.bensa98Label.Name = "bensa98Label";
            this.bensa98Label.Size = new System.Drawing.Size(134, 27);
            this.bensa98Label.TabIndex = 26;
            this.bensa98Label.Text = "Bensa 95";
            // 
            // dieselLabel
            // 
            this.dieselLabel.AutoSize = true;
            this.dieselLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.dieselLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dieselLabel.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieselLabel.Location = new System.Drawing.Point(350, 93);
            this.dieselLabel.Name = "dieselLabel";
            this.dieselLabel.Size = new System.Drawing.Size(134, 27);
            this.dieselLabel.TabIndex = 27;
            this.dieselLabel.Text = "Bensa 95";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(496, 272);
            this.Controls.Add(this.dieselLabel);
            this.Controls.Add(this.bensa98Label);
            this.Controls.Add(this.bensa95Label);
            this.Controls.Add(this.dieselVahissaLabel);
            this.Controls.Add(this.bensa98VahissaLabel);
            this.Controls.Add(this.bensa95VahissaLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tilaaLisaaButton);
            this.Controls.Add(this.dieselVahissaBox);
            this.Controls.Add(this.bensa98VahissaBox);
            this.Controls.Add(this.bensa95VahissaBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dieselBox);
            this.Controls.Add(this.bensa98Box);
            this.Controls.Add(this.bensa95Box);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button tilaaLisaaButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox bensa98Box;
        private System.Windows.Forms.Label bensa98VahissaBox;
        private System.Windows.Forms.Label bensa95VahissaBox;
        private System.Windows.Forms.TextBox bensa95Box;
        private System.Windows.Forms.TextBox dieselBox;
        private System.Windows.Forms.Label dieselVahissaBox;
        private System.Windows.Forms.Label bensa95VahissaLabel;
        private System.Windows.Forms.Label bensa98VahissaLabel;
        private System.Windows.Forms.Label dieselVahissaLabel;
        private System.Windows.Forms.Label bensa95Label;
        private System.Windows.Forms.Label bensa98Label;
        private System.Windows.Forms.Label dieselLabel;
    }
}


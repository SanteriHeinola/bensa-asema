﻿namespace WindowsFormsApp2
{
    partial class tilaaLisaaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tilausButton = new System.Windows.Forms.Button();
            this.bensa95Label = new System.Windows.Forms.Label();
            this.bensa98Label = new System.Windows.Forms.Label();
            this.dieselLabel = new System.Windows.Forms.Label();
            this.bensa95Box = new System.Windows.Forms.TextBox();
            this.bensa98Box = new System.Windows.Forms.TextBox();
            this.dieselBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tilausButton
            // 
            this.tilausButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.tilausButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tilausButton.Location = new System.Drawing.Point(12, 268);
            this.tilausButton.Name = "tilausButton";
            this.tilausButton.Size = new System.Drawing.Size(172, 42);
            this.tilausButton.TabIndex = 0;
            this.tilausButton.Text = "Lähetä tilaus";
            this.tilausButton.UseVisualStyleBackColor = false;
            this.tilausButton.Click += new System.EventHandler(this.tilausButton_Click);
            // 
            // bensa95Label
            // 
            this.bensa95Label.AutoSize = true;
            this.bensa95Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa95Label.Location = new System.Drawing.Point(30, 19);
            this.bensa95Label.Name = "bensa95Label";
            this.bensa95Label.Size = new System.Drawing.Size(121, 29);
            this.bensa95Label.TabIndex = 1;
            this.bensa95Label.Text = "Bensa 95";
            // 
            // bensa98Label
            // 
            this.bensa98Label.AutoSize = true;
            this.bensa98Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bensa98Label.Location = new System.Drawing.Point(30, 106);
            this.bensa98Label.Name = "bensa98Label";
            this.bensa98Label.Size = new System.Drawing.Size(121, 29);
            this.bensa98Label.TabIndex = 2;
            this.bensa98Label.Text = "Bensa 98";
            // 
            // dieselLabel
            // 
            this.dieselLabel.AutoSize = true;
            this.dieselLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieselLabel.Location = new System.Drawing.Point(44, 181);
            this.dieselLabel.Name = "dieselLabel";
            this.dieselLabel.Size = new System.Drawing.Size(88, 29);
            this.dieselLabel.TabIndex = 3;
            this.dieselLabel.Text = "Diesel";
            // 
            // bensa95Box
            // 
            this.bensa95Box.Location = new System.Drawing.Point(37, 62);
            this.bensa95Box.Name = "bensa95Box";
            this.bensa95Box.Size = new System.Drawing.Size(100, 20);
            this.bensa95Box.TabIndex = 4;
            // 
            // bensa98Box
            // 
            this.bensa98Box.Location = new System.Drawing.Point(37, 149);
            this.bensa98Box.Name = "bensa98Box";
            this.bensa98Box.Size = new System.Drawing.Size(100, 20);
            this.bensa98Box.TabIndex = 5;
            // 
            // dieselBox
            // 
            this.dieselBox.Location = new System.Drawing.Point(37, 224);
            this.dieselBox.Name = "dieselBox";
            this.dieselBox.Size = new System.Drawing.Size(100, 20);
            this.dieselBox.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(243, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 39);
            this.button1.TabIndex = 7;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(340, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 39);
            this.button2.TabIndex = 8;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(243, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(58, 39);
            this.button3.TabIndex = 9;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(340, 78);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(58, 39);
            this.button4.TabIndex = 10;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(243, 137);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(58, 39);
            this.button5.TabIndex = 11;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(340, 130);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(58, 39);
            this.button6.TabIndex = 12;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(243, 182);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(58, 39);
            this.button7.TabIndex = 13;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(340, 182);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(58, 39);
            this.button8.TabIndex = 14;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(292, 227);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(58, 39);
            this.button9.TabIndex = 15;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // tilaaLisaaForm
            // 
            this.ClientSize = new System.Drawing.Size(449, 322);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dieselBox);
            this.Controls.Add(this.bensa98Box);
            this.Controls.Add(this.bensa95Box);
            this.Controls.Add(this.dieselLabel);
            this.Controls.Add(this.bensa98Label);
            this.Controls.Add(this.bensa95Label);
            this.Controls.Add(this.tilausButton);
            this.Name = "tilaaLisaaForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Button tilausButton;
        private System.Windows.Forms.Label bensa95Label;
        private System.Windows.Forms.Label bensa98Label;
        private System.Windows.Forms.Label dieselLabel;
        private System.Windows.Forms.TextBox bensa95Box;
        private System.Windows.Forms.TextBox bensa98Box;
        private System.Windows.Forms.TextBox dieselBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
    }
}